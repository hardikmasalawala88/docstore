﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using DocStore.Authorization.Roles;
using DocStore.Authorization.Users;
using DocStore.MultiTenancy;
using DocStore.Categories;
using DocStore.Documents;

namespace DocStore.EntityFrameworkCore
{
    public class DocStoreDbContext : AbpZeroDbContext<Tenant, Role, User, DocStoreDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public DbSet<CategoriesEntity> Categories { get; set; }
        public DbSet<DocumentsEntity> Documents { get; set; }
        public DocStoreDbContext(DbContextOptions<DocStoreDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CategoriesEntity>(e =>
            {
                e.HasIndex(c => c.Name);
                e.HasIndex(c => c.IsActive);
            });

                modelBuilder.Entity<DocumentsEntity>(e =>
                {
                    e.Property(e => e.CategoriesId).HasDefaultValueSql("1");
                    e.HasOne(d => d.Category)
                       .WithMany(p => p.Documents)
                       .HasForeignKey(d => d.CategoriesId)
                       .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
