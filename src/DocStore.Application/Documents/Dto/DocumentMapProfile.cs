﻿using AutoMapper;

namespace DocStore.Documents.Dto
{
    public class DocumentsMapProfile : Profile
    {
        public DocumentsMapProfile()
        {
            CreateMap<DocumentsDto, DocumentsEntity>();
            CreateMap<DocumentsDto, DocumentsEntity>()
                .ForMember(x => x.CreationTime, opt => opt.Ignore());

            CreateMap<CreateDocumentsDto, DocumentsEntity>();
            CreateMap<CreateDocumentsDto, DocumentsEntity>();
        }
    }
}
