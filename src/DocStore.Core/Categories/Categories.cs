﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using Abp.Timing;
using Abp.Domain.Entities;
using System.Collections.Generic;
using DocStore.Documents;

namespace DocStore.Categories
{
    public class CategoriesEntity : FullAuditedEntity<int>
    {
        public CategoriesEntity()
        {
            CreationTime = Clock.Now;
            Documents = new HashSet<DocumentsEntity>();
        }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        public bool IsActive { get; set; } = true;

        public virtual ICollection<DocumentsEntity> Documents { get; set; }
    }
}


