import {
  Component,
  Injector,
  OnInit,
  Output,
  EventEmitter
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { AppComponentBase } from '@shared/app-component-base';
import {
  CategoriesServiceProxy,
  CategoriesDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './edit-categories-dialog.component.html'
})
export class EditCategoriesDialogComponent extends AppComponentBase  
  implements OnInit {
  saving = false;
  Category: CategoriesDto = new CategoriesDto();
  id: number;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _CategoriesService: CategoriesServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._CategoriesService.get(this.id).subscribe((result: CategoriesDto) => {
      this.Category = result;
    });
  }

  save(): void {
    this.saving = true;

    this._CategoriesService
      .update(this.Category)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
