﻿using Abp.Application.Services;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using DocStore.Authorization;
using DocStore.Categories.Dto;
using System.Linq;
using Abp.Linq.Extensions;

namespace DocStore.Categories
{

    [AbpAuthorize(PermissionNames.Pages_Categories)]
    public class CategoriesAppService : AsyncCrudAppService<CategoriesEntity, CategoriesDto, int, PagedCategoriesResultRequestDto, CreateCategoriesDto, CategoriesDto>, ICategoriesAppService
    {
        public CategoriesAppService(IRepository<CategoriesEntity> repository)
             : base(repository)
        {
        }
        protected override IQueryable<CategoriesEntity> CreateFilteredQuery(PagedCategoriesResultRequestDto input)
        {
            return Repository.GetAll()
            .WhereIf(!input.Keyword.IsNullOrWhiteSpace(), x => x.Name.Contains(input.Keyword))
            .WhereIf(input.IsActive.HasValue, x => x.IsActive == input.IsActive);
        }
    }
}

