﻿using Abp.AspNetCore;
using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using DocStore.EntityFrameworkCore;
using DocStore.Web.Startup;
using Microsoft.AspNetCore.Mvc.ApplicationParts;

namespace DocStore.Web.Tests
{
    [DependsOn(
        typeof(DocStoreWebMvcModule),
        typeof(AbpAspNetCoreTestBaseModule)
    )]
    public class DocStoreWebTestModule : AbpModule
    {
        public DocStoreWebTestModule(DocStoreEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbContextRegistration = true;
        } 
        
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DocStoreWebTestModule).GetAssembly());
        }
        
        public override void PostInitialize()
        {
            IocManager.Resolve<ApplicationPartManager>()
                .AddApplicationPartsIfNotAddedBefore(typeof(DocStoreWebMvcModule).Assembly);
        }
    }
}