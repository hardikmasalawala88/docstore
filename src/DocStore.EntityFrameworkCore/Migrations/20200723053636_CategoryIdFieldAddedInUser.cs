﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DocStore.Migrations
{
    public partial class CategoryIdFieldAddedInUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<short>(
                name: "CategoryId",
                table: "AbpUsers",
                nullable: false,
                defaultValue: (short)0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "AbpUsers");
        }
    }
}
