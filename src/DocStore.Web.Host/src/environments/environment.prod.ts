// "Production" enabled environment

export const environment = {
    production: true,
    hmr: false,
    appConfig: 'appconfig.production.json',
    hostUrl: "http://servicelayer.darraenterprises.com",
    uploadDocumentFolder: "/Upload/"
};
