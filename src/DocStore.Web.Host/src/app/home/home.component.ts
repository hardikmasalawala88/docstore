import { Component, Injector, OnInit } from '@angular/core';
import { AppComponentBase } from '@shared/app-component-base';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { UserServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './home.component.html',
  animations: [appModuleAnimation()],
})
export class HomeComponent extends AppComponentBase implements OnInit {
  documentCount: number =0;
  constructor(injector: Injector
    ,private userServiceProxy: UserServiceProxy) {
    super(injector);
  }
  ngOnInit(): void {
    this.setTopLables()
  }
  setTopLables():void
  {
    abp.ui.setBusy();
    this.userServiceProxy.GetDashboardData().subscribe((result) => {
      this.documentCount = result.result.totalDocument;
      abp.ui.clearBusy();
    });
  }

}
