import {
  Component,
  Injector,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { AppComponentBase } from '@shared/app-component-base';
import {
  DocumentsServiceProxy,
  DocumentsDto
} from '@shared/service-proxies/service-proxies';
import { environment } from '../../../environments/environment';

@Component({
  templateUrl: './view-documents-dialog.component.html'
})
export class ViewDocumentsDialogComponent extends AppComponentBase  
  implements OnInit {
  saving = false;
  document: DocumentsDto = new DocumentsDto();
  id: number;
  environment : any = environment;
  @Output() onSave = new EventEmitter<any>();
  url :string;
  constructor(
    injector: Injector,
    public _DocumentsService: DocumentsServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    
    super(injector);
    
    abp.ui.clearBusy();
    
    this.url= environment.hostUrl + environment.uploadDocumentFolder + this.document.GeneratedFileName;
  }

  ngOnInit(): void {
    //abp.ui.setBusy();
    // this._DocumentsService.get(this.id).subscribe((result: DocumentsDto) => {
    //   this.document = result;
    //   abp.ui.clearBusy();
    // });
  }

  @ViewChild('videoPlayer') videoplayer: ElementRef;
  toggleVideo(event: any) {
    const video: HTMLVideoElement = this.videoplayer.nativeElement;
    video.play();
  }

  onCloseClick() {
    this.bsModalRef.hide()
    if(this.videoplayer){
      const video: HTMLVideoElement = this.videoplayer.nativeElement;
      video.pause();
    }
  }
}
