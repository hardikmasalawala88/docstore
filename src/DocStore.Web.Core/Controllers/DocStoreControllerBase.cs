using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace DocStore.Controllers
{
    public abstract class DocStoreControllerBase: AbpController
    {
        protected DocStoreControllerBase()
        {
            LocalizationSourceName = DocStoreConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
