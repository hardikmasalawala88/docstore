﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DocStore.Sessions.Dto;

namespace DocStore.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
