import { DocStoreTemplatePage } from './app.po';

describe('DocStore App', function() {
  let page: DocStoreTemplatePage;

  beforeEach(() => {
    page = new DocStoreTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
