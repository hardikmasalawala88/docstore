﻿using System.Threading.Tasks;
using DocStore.Configuration.Dto;

namespace DocStore.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
