using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using DocStore.Authorization.Users;

namespace DocStore.Dashboard.Dto
{
    
    public class DashboardDto 
    {
        public long TotalDocument { get; set; }

    }

}
