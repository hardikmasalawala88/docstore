import { Component, OnInit, Injector } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { PagedRequestDto, PagedListingComponentBase } from '@shared/paged-listing-component-base';
import { CategoriesDto, CategoriesServiceProxy, CategoriesDtoPagedResultDto } from '@shared/service-proxies/service-proxies';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { CreateCategoriesDialogComponent } from './create-categories/create-categories-dialog.component';
import { EditCategoriesDialogComponent } from './edit-categories/edit-categories-dialog.component';
class PagedCategoriesRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

@Component({
  // selector: 'app-categories',
  templateUrl: './categories.component.html',
  // styleUrls: ['./categories.component.css'],
  animations: [appModuleAnimation()]
})
export class CategoriesComponent extends PagedListingComponentBase<CategoriesDto>  {
  categoriesList: CategoriesDto[] = [];
  keyword = '';
  isActive: boolean | null;
  advancedFiltersVisible = false;

  constructor( injector: Injector,
    private _CategoriesService: CategoriesServiceProxy,
    private _modalService: BsModalService) { 
      super(injector);
    }

  list(
    request: PagedCategoriesRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
    request.keyword = this.keyword;
    request.isActive = this.isActive;

    this._CategoriesService
      .getAll(
        request.keyword,
        request.isActive,
        request.skipCount,
        request.maxResultCount
      )
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: CategoriesDtoPagedResultDto) => {
        this.categoriesList = result.items;
        this.showPaging(result, pageNumber);
      });
  }
  delete(tenant: CategoriesDto): void {
    abp.message.confirm(
      this.l('TenantDeleteWarningMessage', tenant.name),
      undefined,
      (result: boolean) => {
        if (result) {
          this._CategoriesService
            .delete(tenant.id)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l('SuccessfullyDeleted'));
                this.refresh();
              })
            )
            .subscribe(() => {});
        }
      }
    );
  }
  createCategories(): void {
    this.showCreateOrEditCategoriesDialog();
  }

  editCategories(tenant: CategoriesDto): void {
    this.showCreateOrEditCategoriesDialog(tenant.id);
  }
  showCreateOrEditCategoriesDialog(id?: number): void {
    let createOrEditCategoriesDialog: BsModalRef;
    if (!id) {
      createOrEditCategoriesDialog = this._modalService.show(
        CreateCategoriesDialogComponent,
        {
          class: 'modal-lg',
        }
      );
    } else {
      createOrEditCategoriesDialog = this._modalService.show(
        EditCategoriesDialogComponent,
        {
          class: 'modal-lg',
          initialState: {
            id: id,
          },
        }
      );
    }

    createOrEditCategoriesDialog.content.onSave.subscribe(() => {
      this.refresh();
    });
  }

  clearFilters(): void {
    this.keyword = '';
    this.isActive = undefined;
    this.getDataPage(1);
  }
}
