﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using DocStore.Configuration.Dto;

namespace DocStore.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : DocStoreAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
