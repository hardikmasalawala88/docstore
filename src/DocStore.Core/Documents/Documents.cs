﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations;
using Abp.Timing;
using Abp.Domain.Entities;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using DocStore.Categories;
using System.ComponentModel;

namespace DocStore.Documents
{
    public class DocumentsEntity : FullAuditedEntity<int>
    {
        public DocumentsEntity()
        {
            CreationTime = Clock.Now;
        }
        
        [Required]
        public string FileName { get; set; }
        public string GeneratedFileName { get; set; }
        public int FileSize { get; set; }
        public string FileType { get; set; }
        public bool IsActive { get; set; } = true;
        public int CategoriesId { get; set; } = 1;

        public virtual CategoriesEntity Category { get; set; }
    }
}


