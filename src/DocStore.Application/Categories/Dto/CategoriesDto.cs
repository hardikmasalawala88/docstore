using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using DocStore.Authorization.Users;

namespace DocStore.Categories.Dto
{
    [AutoMapFrom(typeof(CategoriesEntity))]
    public class CategoriesDto : EntityDto<int>
    {
        [Required]
        [StringLength(AbpUserBase.MaxNameLength)]
        public string Name { get; set; }
        public bool IsActive { get; set; } = true;
        public DateTime CreationTime { get; set; }

    }

    //custom PagedResultRequestDto
    public class PagedCategoriesResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
        public bool? IsActive { get; set; }
    }
}
