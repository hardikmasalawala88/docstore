﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DocStore.Migrations
{
    public partial class CategoryIdFieldAddedInDocument : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<short>(
                name: "CategoryId",
                table: "Documents",
                nullable: false,
                defaultValue: (short)0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Documents");
        }
    }
}
