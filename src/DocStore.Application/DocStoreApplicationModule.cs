﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using DocStore.Authorization;
using DocStore.Documents;
using DocStore.Documents.Dto;

namespace DocStore
{
    [DependsOn(
        typeof(DocStoreCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class DocStoreApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<DocStoreAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(DocStoreApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(cfg =>
            {
                cfg.AddMaps(thisAssembly);

                cfg.CreateMap<DocumentsEntity, DocumentsDto>()
                      .ForMember(u => u.CategoriesName, options => options.MapFrom(input => input.Category.Name));
            });

            //Configuration.Modules.AbpAutoMapper().Configurators.Add(
            //    // Scan the assembly for classes which inherit from AutoMapper.Profile
            //    cfg => cfg.AddMaps(thisAssembly)
            //);
        }
    }
}
