using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace DocStore.Documents.Dto
{
    [AutoMapTo(typeof(DocumentsEntity))]
    public class CreateDocumentsDto
    {
        [Required]
        public string FileName { get; set; }
        public string GeneratedFileName { get; set; }
        public int FileSize { get; set; }
        public string FileType { get; set; }
        public bool IsActive { get; set; } = true;
        public int CategoriesId { get; set; }
    }
}
