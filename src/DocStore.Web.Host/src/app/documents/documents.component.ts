import { Component, OnInit, Injector } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { PagedRequestDto, PagedListingComponentBase } from '@shared/paged-listing-component-base';
import { DocumentsDto, DocumentsServiceProxy, DocumentsDtoPagedResultDto } from '@shared/service-proxies/service-proxies';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { CreateDocumentsDialogComponent } from './create-documents/create-documents-dialog.component';
import { EditDocumentsDialogComponent } from './edit-documents/edit-documents-dialog.component';
import { ViewDocumentsDialogComponent } from './view-documents/view-documents-dialog.component';
import { environment } from '../../environments/environment';
import { saveAs } from 'file-saver';
class PagedDocumentsRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

@Component({
  templateUrl: './documents.component.html',
  animations: [appModuleAnimation()]
})
export class DocumentsComponent extends PagedListingComponentBase<DocumentsDto>  {
  documentsList: DocumentsDto[] = [];
  keyword = '';
  isActive: boolean | null;
  advancedFiltersVisible = false;
  hostUrl = environment.hostUrl;
  constructor( injector: Injector,
    private _DocumentsService: DocumentsServiceProxy,
    private _modalService: BsModalService
    ) { 
      super(injector);
    }

  list(
    request: PagedDocumentsRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
    request.keyword = this.keyword;
    request.isActive = this.isActive;

    this._DocumentsService
      .getAll(
        request.keyword,
        request.isActive,
        request.skipCount,
        request.maxResultCount
      )
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: DocumentsDtoPagedResultDto) => {
        this.documentsList = result.items;
        this.showPaging(result, pageNumber);
      });
  }
  delete(document: DocumentsDto): void {
    abp.message.confirm(
      this.l(document.FileName +' will be deleted.'),
      undefined,
      (result: boolean) => {
        if (result) {
          this._DocumentsService
            .delete(document.id)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l('SuccessfullyDeleted'));
                this.refresh();
              })
            )
            .subscribe(() => {});
        }
      }
    );
  }
  documentHideUnhide(document: DocumentsDto): void {
    var Message = "'" + document.FileName + '\' document will be ' + (document.IsActive ? 'hide' : 'unhide') + '.';
    abp.message.confirm(
      this.l(Message),
      undefined,
      (result: boolean) => {
        if (result) {
          this._DocumentsService
            .documentHideUnhide(document.id)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l('SuccessfullyDeleted'));
                this.refresh();
              })
            )
            .subscribe(() => {});
        }
      }
    );
  }
  createDocuments(): void {
    this.showCreateOrEditDocumentsDialog();
  }
  editDocuments(tenant: DocumentsDto): void {
    this.showCreateOrEditDocumentsDialog(tenant.id);
  }
  showCreateOrEditDocumentsDialog(id?: number): void {
    let createOrEditDocumentsDialog: BsModalRef;
    if (!id) {
      createOrEditDocumentsDialog = this._modalService.show(
        CreateDocumentsDialogComponent,
        {
          class: 'modal-lg', ignoreBackdropClick: true, backdrop: true, keyboard: false, focus: true,
        }
      );
    } else {
      createOrEditDocumentsDialog = this._modalService.show(
        EditDocumentsDialogComponent,
        {
          class: 'modal-lg', ignoreBackdropClick: true, backdrop: true, keyboard: false, focus: true,
          initialState: {
            id: id,
          },
        }
      );
    }

    createOrEditDocumentsDialog.content.onSave.subscribe(() => {
      this.refresh();
    });
  }
  viewDocuments(document: DocumentsDto): void{
    abp.ui.setBusy();
    var docURL = this.hostUrl + environment.uploadDocumentFolder  +document.GeneratedFileName;
    let viewDocumentsDialog: BsModalRef;
    if (document) {
      viewDocumentsDialog = this._modalService.show(
        ViewDocumentsDialogComponent,
        {
          class: 'modal-xl', ignoreBackdropClick: true, backdrop: true, keyboard: false, focus: true,
          initialState: {
            id: document.id,
            document:document
          },
        }
      );
    } 
  }
  HardDeleteDocuments(document: DocumentsDto): void{
    abp.message.confirm(
      this.l(document.FileName +' will be deleted.'),
      undefined,
      (result: boolean) => {
        if (result) {
          this._DocumentsService
            .DeleteFile(document)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l('SuccessfullyDeleted'));
                this.refresh();
              })
            )
            .subscribe(() => {});
        }
      }
    );
  }
  downloadDocument(objDoc: DocumentsDto){
    let linkurl= environment.hostUrl + environment.uploadDocumentFolder + objDoc.GeneratedFileName;
    saveAs(linkurl, objDoc.FileName);
    // FileSaver.saveAs("https://httpbin.org/image", "image.jpg");
      // // const link = document.createElement('a');
      // // link.setAttribute('target', '_blank');
      // // link.setAttribute('href',environment.hostUrl + environment.uploadDocumentFolder + objDoc.GeneratedFileName);
      // // link.setAttribute('download','');
      // // document.body.appendChild(link);
      // // link.click();
      // // link.remove();
      // const link = document.createElement('a');
      // link.setAttribute('target', '_blank');
      // link.setAttribute('href',environment.hostUrl + environment.uploadDocumentFolder + objDoc.GeneratedFileName);
      // link.setAttribute('download', objDoc.FileName);
      // document.body.appendChild(link);
      // link.click();
      // link.remove();

      // let link = document.createElement('a');
      // link.setAttribute('type', 'hidden');
      // link.href = 'assets/file';
      // link.download = linkurl;
      // document.body.appendChild(link);
      // link.click();
      // link.remove();
     
}
  clearFilters(): void {
    this.keyword = '';
    this.isActive = undefined;
    this.getDataPage(1);
  }

  isControlVisible(permissionName: any): boolean {
    if (!permissionName) {
      return true;
    }
    return this.permission.isGranted(permissionName);
  }
  
}
