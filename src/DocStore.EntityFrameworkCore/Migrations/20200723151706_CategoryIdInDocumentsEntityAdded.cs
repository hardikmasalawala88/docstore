﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DocStore.Migrations
{
    public partial class CategoryIdInDocumentsEntityAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CategoriesId",
                table: "Documents",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CategoryId",
                table: "Documents",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Documents_CategoriesId",
                table: "Documents",
                column: "CategoriesId");

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_Categories_CategoriesId",
                table: "Documents",
                column: "CategoriesId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Documents_Categories_CategoriesId",
                table: "Documents");

            migrationBuilder.DropIndex(
                name: "IX_Documents_CategoriesId",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "CategoriesId",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Documents");
        }
    }
}
