﻿using Abp.Application.Services;
using DocStore.MultiTenancy.Dto;

namespace DocStore.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

