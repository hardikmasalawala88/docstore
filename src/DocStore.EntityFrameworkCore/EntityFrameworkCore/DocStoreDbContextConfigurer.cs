using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace DocStore.EntityFrameworkCore
{
    public static class DocStoreDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<DocStoreDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<DocStoreDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
