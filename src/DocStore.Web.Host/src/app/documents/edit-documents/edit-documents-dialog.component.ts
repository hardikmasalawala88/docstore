import {
  Component,
  Injector,
  OnInit,
  Output,
  EventEmitter
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { AppComponentBase } from '@shared/app-component-base';
import {
  DocumentsServiceProxy,
  DocumentsDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './edit-documents-dialog.component.html'
})
export class EditDocumentsDialogComponent extends AppComponentBase  
  implements OnInit {
  saving = false;
  document: DocumentsDto = new DocumentsDto();
  id: number;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _DocumentsService: DocumentsServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this._DocumentsService.get(this.id).subscribe((result: DocumentsDto) => {
      this.document = result;
    });
  }

  save(): void {
    this.saving = true;

    this._DocumentsService
      .update(this.document)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
