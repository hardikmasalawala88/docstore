﻿namespace DocStore
{
    public class DocStoreConsts
    {
        public const string LocalizationSourceName = "DocStore";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = false;
    }
}
