﻿using Abp.Authorization;
using DocStore.Authorization.Roles;
using DocStore.Authorization.Users;

namespace DocStore.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
