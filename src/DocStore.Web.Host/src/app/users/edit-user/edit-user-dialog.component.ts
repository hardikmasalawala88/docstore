import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  UserServiceProxy,
  UserDto,
  RoleDto,
  CategoriesServiceProxy,
  CategoriesDtoPagedResultDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './edit-user-dialog.component.html'
})
export class EditUserDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  user = new UserDto();
  roles: RoleDto[] = [];
  checkedRolesMap: { [key: string]: boolean } = {};
  id: number;
//DROPDOWN
CategoryList = []; 
  
//DROPDOWN

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _userService: UserServiceProxy,
    public bsModalRef: BsModalRef,
    private _CategoriesService: CategoriesServiceProxy,
  ) {
    super(injector);
  }

  ngOnInit(): void {
    abp.ui.setBusy();
    this._userService.get(this.id).subscribe((result) => {
      this.user = result;

      this._CategoriesService
      .getAll('',true,0,10)
      .subscribe((result: CategoriesDtoPagedResultDto) => {
        this.CategoryList = result.items;
        abp.ui.clearBusy();
      });
      abp.ui.setBusy();
      this._userService.getRoles().subscribe((result2) => {
        this.roles = result2.items;
        abp.ui.clearBusy();
        this.setInitialRolesStatus();
      });
    });
    

  }

  setInitialRolesStatus(): void {
    _.map(this.roles, (item) => {
      this.checkedRolesMap[item.normalizedName] = this.isRoleChecked(
        item.normalizedName
      );
    });
  }

  isRoleChecked(normalizedName: string): boolean {
    return _.includes(this.user.roleNames, normalizedName);
  }

  onRoleChange(role: RoleDto, $event) {
    this.checkedRolesMap[role.normalizedName] = $event.target.checked;
  }

  getCheckedRoles(): string[] {
    const roles: string[] = [];
    _.forEach(this.checkedRolesMap, function (value, key) {
      if (value) {
        roles.push(key);
      }
    });
    return roles;
  }

  save(): void {
    this.saving = true;
    abp.ui.setBusy();
    this.user.roleNames = this.getCheckedRoles();
    this._userService
      .update(this.user)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        abp.ui.clearBusy();
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
  
}
