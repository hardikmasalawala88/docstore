import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  CreateCategoriesDto,
  CategoriesServiceProxy
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './create-categories-dialog.component.html'
})
export class CreateCategoriesDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  category: CreateCategoriesDto = new CreateCategoriesDto();

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _categoriesService: CategoriesServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
  }

  save(): void {
    this.saving = true;

    this._categoriesService
      .create(this.category)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}
