import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output
} from '@angular/core';
import { finalize } from 'rxjs/operators';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  CreateDocumentsDto,
  DocumentsServiceProxy,
  CategoriesServiceProxy,
  CategoriesDtoPagedResultDto,
  CategoriesDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './create-documents-dialog.component.html'
})
export class CreateDocumentsDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  isTableLoading = false;
  document: CreateDocumentsDto = new CreateDocumentsDto();
//File Start
file: any;
//File End
//DROPDOWN
CategoryList :any = []; 
SkipCategories: number =0;
TakeCategories: number =100;
LoadMoreCategories :boolean = true;
//DROPDOWN
  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _documentsService: DocumentsServiceProxy,
    private _CategoriesService: CategoriesServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.isTableLoading = true;
    this._documentsService.GetAllCategories(this.SkipCategories,this.TakeCategories)
    .subscribe((result) => {
      this.CategoryList = result.result;
      this.isTableLoading = false;
    });
  }

  save(): void {
    if (!this.file || this.file.length === 0) {
      this.notify.error(this.l('Please select document.'));
      return;
    }
    if (!this.document.CategoryId || this.document.CategoryId == 0) {
      this.notify.error(this.l('Please select document\'s category.'));
        return ;
    }
    
    for (let sfile of this.file) {
      if (this.bytesToMegaBytes(sfile.size) > 10) {
        this.notify.error(this.l('Please attach file with less than 10MB size.'));
        return;
      }
    }

    this.saving = true;
    abp.ui.setBusy();
    this._documentsService
      .UploadFile(this.document, this.file)
      .pipe(
        finalize(() => {
         this.saving = false;
        })
      )
      .subscribe((data) => {
        if(data.success == true){
          abp.ui.clearBusy();
          this.notify.info(this.l('Uploaded Successfully'));
          this.bsModalRef.hide();
          this.onSave.emit();
        }else{
          abp.ui.clearBusy();
          this.notify.info(this.l('Uploaded Failed. Please try again.'));
          this.bsModalRef.hide();
          this.onSave.emit();
          this.file = null;
        }

      });
  }

//File Start
onSelectFile($event, file) {
    this.file = file;
  }
  bytesToMegaBytes(bytes) { 
    return (bytes / (1024*1024)); 
  }
  //File End
}
