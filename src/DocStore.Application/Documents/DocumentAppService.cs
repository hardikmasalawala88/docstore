﻿using Abp.Application.Services;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using DocStore.Authorization;
using DocStore.Documents.Dto;
using System.Linq;
using Abp.Linq.Extensions;
using DocStore.Authorization.Users;
using System;
using Abp.ObjectMapping;
using DocStore.Authorization.Roles;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using System.Linq.Dynamic.Core;
using DocStore.Categories;
using DocStore.Categories.Dto;
using System.Collections.Generic;
using DocStore.Dashboard.Dto;

namespace DocStore.Documents
{

    [AbpAuthorize(PermissionNames.Pages_Documents)]
    public class DocumentsAppService : AsyncCrudAppService<DocumentsEntity, DocumentsDto, int, PagedDocumentsResultRequestDto, CreateDocumentsDto, DocumentsDto>, IDocumentsAppService
    {
        
        private readonly UserManager _userManager;
        private readonly IRepository<Role> _RoleRepository;
        private readonly IRepository<CategoriesEntity> _CategoryRepository;
        
        private readonly IObjectMapper _objectMapper;
        public DocumentsAppService(IRepository<DocumentsEntity> repository
            , UserManager userManager
            , IObjectMapper objectMapper
            , IRepository<Role> roleRepository
            ,IRepository<CategoriesEntity> categoryRepository)
             : base(repository)
        {
            _userManager = userManager;
            _objectMapper = objectMapper;
            _RoleRepository = roleRepository;
            _CategoryRepository = categoryRepository;
        }
        protected override IQueryable<DocumentsEntity> CreateFilteredQuery(PagedDocumentsResultRequestDto input)
        {
            var user = _userManager.Users.Include(x => x.Roles).First(x => x.Id == AbpSession.UserId);
            var userRole = _RoleRepository.Get(user.Roles.First().RoleId);

            var documentQuery = Repository.GetAllIncluding(r => r.Category).Where(r => r.CategoriesId == r.Category.Id).WhereIf(!input.Keyword.IsNullOrWhiteSpace(), x => x.FileName.Contains(input.Keyword));

            if (userRole.NormalizedName.Equals("ADMIN"))
            {
                documentQuery = documentQuery.WhereIf(input.IsActive.HasValue, x => x.IsActive == input.IsActive);
            }
            else
            {
                documentQuery = documentQuery.Where(x => x.IsActive).Where(x => x.CategoriesId == user.CategoryId);
            }

            return documentQuery;
        }

        public async System.Threading.Tasks.Task<DocumentsEntity>  DocumentHideUnhide(int id)
        {
            var documentEntity = await Repository.GetAsync(id);
            documentEntity.IsActive = !documentEntity.IsActive;
            return Repository.Update(_objectMapper.Map<DocumentsEntity>(documentEntity));
        }
        public List<CategoriesEntity> GetAllCategories(int SkipCount, int MaxResultCount)
        {
            var CategoriesList = _CategoryRepository.GetAll().Where(x => x.IsActive == true).Skip(SkipCount).Take(MaxResultCount).ToList();
            return CategoriesList;
        }

        public DashboardDto GetDashboardData()
        {
            var user = _userManager.Users.Include(x => x.Roles).First(x => x.Id == AbpSession.UserId);
            var userRole = _RoleRepository.Get(user.Roles.First().RoleId);

            DashboardDto dashboardDto = new DashboardDto();
            var DocumentCountQuery = Repository.GetAll();
            //dashboardDto.TotalDocument  = Repository.GetAll().Where(x => x.IsActive == true).Count();

            if (userRole.NormalizedName.Equals("ADMIN"))
            {
                dashboardDto.TotalDocument = DocumentCountQuery.Count();
            }
            else
            {
                dashboardDto.TotalDocument = DocumentCountQuery.Where(x => x.IsActive == true && x.CategoriesId == user.CategoryId).Count();
            }
            return dashboardDto;
        }
    }
}

