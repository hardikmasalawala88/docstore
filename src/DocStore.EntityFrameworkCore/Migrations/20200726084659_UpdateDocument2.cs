﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DocStore.Migrations
{
    public partial class UpdateDocument2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CategoriesId",
                table: "Documents",
                nullable: false,
                defaultValueSql: "1");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_CategoriesId",
                table: "Documents",
                column: "CategoriesId");

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_Categories_CategoriesId",
                table: "Documents",
                column: "CategoriesId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Documents_Categories_CategoriesId",
                table: "Documents");

            migrationBuilder.DropIndex(
                name: "IX_Documents_CategoriesId",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "CategoriesId",
                table: "Documents");
        }
    }
}
