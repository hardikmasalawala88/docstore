﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using DocStore.Authorization.Users;
using DocStore.Users.Dto;
using Microsoft.AspNetCore.Identity;

namespace DocStore.MyProfile
{
    public class MyProfileAppService : AsyncCrudAppService<User, UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>, IMyProfileAppService
    {
        private readonly UserManager _userManager;

        public MyProfileAppService(IRepository<User, long> repository,UserManager userManager)
            : base(repository)
        {
            _userManager = userManager;
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public async Task<MyProfileDto> GetMyProfile()
        {
            var user = await _userManager.FindByIdAsync(AbpSession.UserId.ToString());

            return ObjectMapper.Map<MyProfileDto>(user);
        }

        public async Task<MyProfileDto> UpdateMyProfile(UpdateMyProfileInput updateInput)
        {
            var user = await _userManager.GetUserByIdAsync(AbpSession.UserId.Value);
            ObjectMapper.Map(updateInput, user);
            CheckErrors(await _userManager.UpdateAsync(user));
            return await GetMyProfile();
        }
    }
}

