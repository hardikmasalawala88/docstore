﻿using System.Threading.Tasks;
using Abp.Application.Services;
using DocStore.Authorization.Accounts.Dto;
using DocStore.Authorization.Users;

namespace DocStore.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);

        //Task<User> GetUserProflie(long Id);
    }
}
