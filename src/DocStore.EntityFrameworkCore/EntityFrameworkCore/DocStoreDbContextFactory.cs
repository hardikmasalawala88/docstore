﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using DocStore.Configuration;
using DocStore.Web;

namespace DocStore.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class DocStoreDbContextFactory : IDesignTimeDbContextFactory<DocStoreDbContext>
    {
        public DocStoreDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<DocStoreDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            DocStoreDbContextConfigurer.Configure(builder, configuration.GetConnectionString(DocStoreConsts.ConnectionStringName));

            return new DocStoreDbContext(builder.Options);
        }
    }
}
