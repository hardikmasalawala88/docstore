using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace DocStore.Categories.Dto
{
    [AutoMapTo(typeof(CategoriesEntity))]
    public class CreateCategoriesDto
    {
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        public bool IsActive { get; set; } = true;
    }
}
