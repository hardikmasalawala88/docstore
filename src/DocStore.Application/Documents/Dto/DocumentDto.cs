using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using DocStore.Authorization.Users;
using DocStore.Categories;
using Microsoft.AspNetCore.Http;

namespace DocStore.Documents.Dto
{
    [AutoMapFrom(typeof(DocumentsEntity))]
    public class DocumentsDto : EntityDto<int>
    {
        [Required]
        public string FileName { get; set; }
        public string GeneratedFileName { get; set; }
        public int FileSize { get; set; }
        public string FileType { get; set; }
        public bool IsActive { get; set; } = true;
        public int CategoriesId { get; set; }
        public string CategoriesName { get; set; }
        public IFormFile DocumentFile { get; set; }
    }

    //Custom PagedResultRequestDto
    public class PagedDocumentsResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
        public bool? IsActive { get; set; }
    }
}
