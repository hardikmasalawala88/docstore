﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;
using DocStore.Documents;
using DocStore.Documents.Dto;
using System;
using Abp.Runtime.Security;
using Abp.Application.Services.Dto;
using DocStore.Users.Dto;
using System.Threading.Tasks;
using DocStore.Users;

namespace DocStore.Controllers
{
    [Route("api/[controller]/[action]")]
    public class FileUploadController : DocStoreControllerBase
    {
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly IDocumentsAppService _documentsAppService;
        private readonly IUserAppService _userAppService;
        public FileUploadController(IWebHostEnvironment hostingEnvironment
            ,IDocumentsAppService documentsAppService
            ,IUserAppService userAppService)
        {
            _hostingEnvironment = hostingEnvironment;
            _documentsAppService = documentsAppService;
            _userAppService = userAppService;
        }
       

        [HttpPost, DisableRequestSizeLimit]
        public async System.Threading.Tasks.Task<ObjectResult> UploadFileAsync()
        {
            var file = Request.Form.Files[0];
            string newPath = Path.Combine(_hostingEnvironment.WebRootPath, "Upload");
            string newFileName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
            string fullPath = Path.Combine(newPath, newFileName);
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }
            try
            {
                if (file.Length > 0)
                {
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                }
                //Save in DB START
                CreateDocumentsDto createDocumentsDto = new CreateDocumentsDto
                {
                    FileName = file.FileName,
                    FileType = file.ContentType,
                    GeneratedFileName = newFileName,
                    IsActive = true,
                    CategoriesId = 1,
                    FileSize = Convert.ToInt32(file.Length/1024)
                };
                var status = await _documentsAppService.CreateAsync(createDocumentsDto);
                //Save in DB END
                return Ok(newFileName);
            }
            catch (System.Exception ex)
            {
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }
                return BadRequest(ex.Message);
            }
        }
        [HttpPost, DisableRequestSizeLimit]
        public async System.Threading.Tasks.Task<ObjectResult> UploadFileAsyncParam(int categoryId)
        {
            var file = Request.Form.Files[0];
            string newPath = Path.Combine(_hostingEnvironment.WebRootPath, "Upload");
            string fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
            string fullPath = Path.Combine(newPath, fileName);
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }
            try
            {
                if (file.Length > 0)
                {
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        file.CopyTo(stream);
                    }
                }
                //Save in DB START
                CreateDocumentsDto createDocumentsDto = new CreateDocumentsDto
                {
                    FileName = file.FileName,
                    FileType = file.ContentType,
                    GeneratedFileName = fileName,
                    IsActive = true,
                    CategoriesId = categoryId,
                    FileSize = Convert.ToInt32(file.Length/1024)
                };
                var status = await _documentsAppService.CreateAsync(createDocumentsDto);
                //Save in DB END
                return Ok(fileName);
            }
            catch (System.Exception ex)
            {

                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }
                return BadRequest(ex.Message);
            }
        }
        [HttpPost]
        public async Task<ObjectResult> UpdateUserProfileWithAsyncImage([FromForm] UserDto userDto)
        {

            //Update profile pic in progress
            await _userAppService.UpdateAsync(userDto);
            return Ok(userDto);

        }
    }
}
