﻿namespace DocStore.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_Tenants = "Pages.Tenants";

        public const string Pages_Users = "Pages.Users";

        public const string Pages_Roles = "Pages.Roles";

        public const string Pages_Categories = "Pages.Categories";

        public const string Pages_Documents = "Pages.Documents";

        //Operation Permission 
        //Upload, Download, View, Hide , Delete
        public const string Operation_Upload = "Operation.Upload";
        public const string Operation_Download = "Operation.Download";
        public const string Operation_View = "Operation.View";
        public const string Operation_Hide = "Operation.Hide";
        public const string Operation_Delete = "Operation.Delete";
    }
}
