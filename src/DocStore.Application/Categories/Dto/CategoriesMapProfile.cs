﻿using AutoMapper;

namespace DocStore.Categories.Dto
{
    public class CategoriesMapProfile : Profile
    {
        public CategoriesMapProfile()
        {
            CreateMap<CategoriesDto, CategoriesEntity>();
            CreateMap<CategoriesDto, CategoriesEntity>()
                .ForMember(x => x.CreationTime, opt => opt.Ignore());

            CreateMap<CreateCategoriesDto, CategoriesEntity>();
            CreateMap<CreateCategoriesDto, CategoriesEntity>();
        }
    }
}
