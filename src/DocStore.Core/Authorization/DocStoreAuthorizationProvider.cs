﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace DocStore.Authorization
{
    public class DocStoreAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            context.CreatePermission(PermissionNames.Pages_Users, L("Users"));
            context.CreatePermission(PermissionNames.Pages_Roles, L("Roles"));
            context.CreatePermission(PermissionNames.Pages_Categories, L("Categories"));
            context.CreatePermission(PermissionNames.Pages_Documents, L("Documents"));
            context.CreatePermission(PermissionNames.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);

            context.CreatePermission(PermissionNames.Operation_Upload, L("Document Upload"));
            context.CreatePermission(PermissionNames.Operation_Download, L("Document download"));
            context.CreatePermission(PermissionNames.Operation_View, L("Document view"));
            context.CreatePermission(PermissionNames.Operation_Hide, L("Document hide"));
            context.CreatePermission(PermissionNames.Operation_Delete, L("Document delete"));
    }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, DocStoreConsts.LocalizationSourceName);
        }
    }
}
