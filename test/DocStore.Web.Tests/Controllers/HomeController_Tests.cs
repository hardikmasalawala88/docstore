﻿using System.Threading.Tasks;
using DocStore.Models.TokenAuth;
using DocStore.Web.Controllers;
using Shouldly;
using Xunit;

namespace DocStore.Web.Tests.Controllers
{
    public class HomeController_Tests: DocStoreWebTestBase
    {
        [Fact]
        public async Task Index_Test()
        {
            await AuthenticateAsync(null, new AuthenticateModel
            {
                UserNameOrEmailAddress = "admin",
                Password = "123qwe"
            });

            //Act
            var response = await GetResponseAsStringAsync(
                GetUrl<HomeController>(nameof(HomeController.Index))
            );

            //Assert
            response.ShouldNotBeNullOrEmpty();
        }
    }
}