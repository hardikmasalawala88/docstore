﻿using Abp.Application.Services;
using DocStore.Categories;
using DocStore.Documents.Dto;
using System.Collections.Generic;
using System.Linq;

namespace DocStore.Documents
{
    public interface IDocumentsAppService : IAsyncCrudAppService<DocumentsDto, int, PagedDocumentsResultRequestDto, CreateDocumentsDto, DocumentsDto>
    {
        System.Threading.Tasks.Task<DocumentsEntity> DocumentHideUnhide(int id);
        //IQueryable<CategoriesEntity> GetAllCategories(int SkipCategories, int TakeCategories);
        List<CategoriesEntity> GetAllCategories(int SkipCount, int MaxResultCount);
    }
}
