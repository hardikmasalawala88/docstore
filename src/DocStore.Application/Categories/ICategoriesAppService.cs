﻿using Abp.Application.Services;
using DocStore.Categories.Dto;

namespace DocStore.Categories
{
    public interface ICategoriesAppService : IAsyncCrudAppService<CategoriesDto, int, PagedCategoriesResultRequestDto, CreateCategoriesDto, CategoriesDto>
    {
    }
}
