using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using DocStore.Authorization.Users;

namespace DocStore.MyProfile.Dto
{
    [AutoMapFrom(typeof(User))]
    public class MyProfileDto : EntityDto<long>
    {
        public string UserName { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxSurnameLength)]
        public string Surname { get; set; }

        public string EmailAddress { get; set; }
    }
}
