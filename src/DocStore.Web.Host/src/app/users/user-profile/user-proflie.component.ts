import { Component, Injector, OnInit } from '@angular/core';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { AppComponentBase } from '@shared/app-component-base';
import {
    GetMyProfilesDto,
    UpdateMyProfilesDto,
  UserServiceProxy
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: './user-proflie.component.html',
  animations: [appModuleAnimation()]
})
export class UserProflieComponent extends AppComponentBase implements OnInit {
  saving = false;
  isEdit: boolean = false;
    ShowLoaderEditProfile: boolean = false;
    user = new GetMyProfilesDto();
    
  constructor(
    injector: Injector,
      private _userServiceProxy: UserServiceProxy,
  ) {
    super(injector);
  }

  ngOnInit(): void {
      this.get();
    }

    get() {
        abp.ui.setBusy();
        //this.sessionServiceProxy.getCurrentLoginInformations().subscribe((userData) => {
        this._userServiceProxy.getMyProfile().subscribe((result) => {
            
            this.user = result;
            abp.ui.clearBusy();
        });
    //});
    }

  update() {
      //this.ShowLoaderEditProfile = true;
      this.saving = true;
      var updateMyProfilesDto = new UpdateMyProfilesDto();
      updateMyProfilesDto.name = this.user.name;
      updateMyProfilesDto.surname = this.user.surname;
      this._userServiceProxy.updateMyProfile(updateMyProfilesDto).subscribe((success) => {
          if (success) {
          //this.ShowLoaderEditProfile = false;
          this.saving = false;
           abp.message.success('Profile updated successfully', 'Success');
           
         }
      });
  }
}
